import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';



import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { FilmListComponent } from './components/filmlist/filmlist.component';
import { FilmviewComponent } from './components/filmview/filmview.component';

@NgModule({
  declarations: [
    SearchComponent,
    FilmListComponent,
    FilmviewComponent,
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
