import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import {MatIconModule} from '@angular/material/icon';

import * as $ from 'jquery';

@Component({
  selector: 'filmview',
  templateUrl: './filmview.component.html'
})
export class FilmviewComponent {
  constructor( public dataService: DataService ) {}
  close_modal() {
    $(".modal_film").slideUp();
  }
}
