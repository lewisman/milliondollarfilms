import { Component } from '@angular/core'
import * as $ from 'jquery';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'search-box',
    templateUrl: './search.component.html'
})
export class SearchComponent {
    public label: string = "Search";
    public search_input: string = "";
    constructor( public dataService: DataService ) {}

    ngOnInit() {
        
    }

    launchSearch() {
        const value_search = $(".search_film-input").val(),
              mainScope = this;
        $.get( "http://www.omdbapi.com/?apikey=77cacfe9&s="+value_search, function( data ) {
            console.log(data)
            if(data.Response == "True")
                mainScope.dataService.filmsList = data;
        });
    }
}