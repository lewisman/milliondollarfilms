import { Component } from '@angular/core'
import * as $ from 'jquery';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'filmlist',
    templateUrl: './filmlist.component.html'
})
export class FilmListComponent {
    constructor( public dataService: DataService ) {
    }
    ngOnInit() {
        
    }
    show_film(filmID) {
        const mainScope = this;
        $.get( "http://www.omdbapi.com/?apikey=77cacfe9&i="+filmID, function( data ) {
            if(data.Response == "True") {
                mainScope.dataService.film = data;
                $(".modal_film").slideDown();
            }
        });
    }
}